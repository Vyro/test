﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Timeline;

public class Corbeau : MonoBehaviour
{
    public int life;
    public int damage;

    public GameObject hero;
    public LayerMask layerHero;

    public float MoveSpeed = 0.05f;
    private float distanceCast = 5f;
    private Vector2 originCorbeau;
    private Vector2 directionCorbeau;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        originCorbeau = transform.position;
        directionCorbeau = transform.forward;

        if (life <= 0)
        {
            // Detruire le corbeau
            Destroy(this);
        }

        DetectionHero();
    }

    void DetectionHero()
    {
        // Dans Inspector, mettre le layer du héro pour layerHero
        RaycastHit2D hits = Physics2D.CircleCast(originCorbeau, 6, directionCorbeau, distanceCast, layerHero);
        
        // Affiche la zone de détection
        Debug.DrawRay(originCorbeau, directionCorbeau, Color.red); // <- ça marche pas

        if (hits)
        {
            transform.position = Vector3.MoveTowards(transform.position, hero.transform.position, MoveSpeed);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            Attack();
        }
    }

    void Attack()
    {
        Debug.Log("Attack of corbeau");
        if (hero != null)
        {
            hero.GetComponent<Hero>().GetDamage(damage);
        }
        else
        {
            Debug.Log("Erreur : La variable 'hero' est null !");
        }
    }
}