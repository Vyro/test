﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Epouvantail : MonoBehaviour
{
    public Animator animator;

    public bool isEpouvantailAgressiv;

    public int life;
    public int damage;

    public GameObject hero;
    public LayerMask layerHero;

    private float distanceCast = 2f;
    private Vector2 originEpouvantail;
    private Vector2 directionEpouvantail;

    // Start is called before the first frame update
    void Start()
    {
        animator.enabled = false;
    }
    
    void Update()
    {
    }

    /*void DetectionHero()
    {
        // Dans Inspector, mettre le layer du héro pour layerHero
        RaycastHit2D hits = Physics2D.CircleCast(originEpouvantail, 6, directionEpouvantail, distanceCast, layerHero);

        // Affiche la zone de détection
        Debug.DrawRay(originEpouvantail, directionEpouvantail, Color.red); // <- ça marche pas

        if (hits)
        {
            Attack();
        }
    }*/
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            if (isEpouvantailAgressiv)
            {
                Attack();
            }
        }
    }
    
    void Attack()
    {
        Debug.Log("Attack of epouvantail");
        
        // Activation de l'animation
        animator.enabled = true;
        
        if (hero != null)
        {
            hero.GetComponent<Hero>().GetDamage(damage);
        }
        else
        {
            Debug.Log("Erreur : La variable 'hero' est null !");
        }
    }
}