﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoov : MonoBehaviour
{
    public Vector2 minCameraPos;
    public Vector2 maxCameraPos;

    public GameObject gears;

    void Start()
    {
        gears = GameObject.FindGameObjectWithTag("GameController");
    }

   void Update()
   {
       float extraSight;
       
       if (gears.GetComponent<GearsScript>().player.GetComponent<MoovDisplay>().facingLeft)
       {
           extraSight = -1.5f;
       }
       else
       {
           extraSight = 1.5f;
       }
       Vector3 smoothedPosition = Vector3.Lerp(transform.position, new Vector3(gears.GetComponent<GearsScript>().player.transform.position.x + extraSight, gears.GetComponent<GearsScript>().player.transform.position.y + 2 + 2 * Input.GetAxis("Vertical"), transform.position.z), 0.125f);   //Smooth Camera
       
       transform.position = smoothedPosition;
       
       transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCameraPos.x, maxCameraPos.x), Mathf.Clamp(transform.position.y, minCameraPos.y, maxCameraPos.y), transform.position.z);
   }
}
