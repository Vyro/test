﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GearsScript : MonoBehaviour
{
    public Camera cam;
    public GameObject player;
    public Canvas canvasMain;
    public LayerMask platform;
    public Tilemap tileMapMain;
    public GameObject Clock;

    public GameObject nightPanel;
    public float gameTimeMulti = 10f; //tous multiplier pour mettre les heures de l'univers du jeu en minutes IRL(pour le système jour/nuit)

    public float dayTime;

    void Start()
    {
        cam = Camera.main;
        player = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {
        dayTime += Time.deltaTime;//* gameTimeMulti;
        if (dayTime < 7 || dayTime > 21)
        {
           tileMapMain.color = Color.gray;
            nightPanel.SetActive(true);
            player.GetComponent<MoovDisplay>().form = true;
        }
        else
        {
            tileMapMain.color = Color.white;
            nightPanel.SetActive(false);
            player.GetComponent<MoovDisplay>().form = false;
        }

        if (dayTime >= 24)
        {
            dayTime = 0;
        }

        Clock.transform.eulerAngles = new Vector3(0,0,-dayTime * 30);
    }
}
